package com.fellows.patterns._03_behavioral._01_template;

public class HousingClient {

    public static void main(String[] args) {
        HouseTemplate ht = new WoodenHouse();
        ht.buildHouse();

        System.out.println("----------------------------");

        HouseTemplate ht2 = new GlassHouse();
        ht2.buildHouse();
    }
}
