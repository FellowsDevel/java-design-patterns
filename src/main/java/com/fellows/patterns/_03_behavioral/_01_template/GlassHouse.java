package com.fellows.patterns._03_behavioral._01_template;

public class GlassHouse extends HouseTemplate {
    @Override
    public void buildWalls() {
        System.out.println("Construindo paredes de vidro");
    }

    @Override
    public void buildPillars() {
        System.out.println("Construindo pilares revestidos de vidro");
    }
}
