package com.fellows.patterns._03_behavioral._01_template;

public class WoodenHouse extends HouseTemplate {

    @Override
    public void buildWalls() {
        System.out.println("Construindo paredes de madeira");
    }

    @Override
    public void buildPillars() {
        System.out.println("Construindo pilares revestidos de madeira");
    }
}
