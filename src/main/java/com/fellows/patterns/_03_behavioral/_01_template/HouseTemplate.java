package com.fellows.patterns._03_behavioral._01_template;

public abstract class HouseTemplate {

    /**
     * O método é FINAL para não ser sobrescrito
     * <p>
     * Este é o Método Modelo e ele quem define a ordem de execução de diversos passos
     */
    public final void buildHouse() {
        buildFoundation();
        buildPillars();
        buildWalls();
        buildWindows();
        System.out.println("A casa está construída!");
    }

    /**
     * Implementação padrão
     */
    private void buildWindows() {
        System.out.println("Construindo Janelas de Vidro");
    }

    // Métodos para as subclasses implementarem
    public abstract void buildWalls();

    public abstract void buildPillars();

    private void buildFoundation() {
        System.out.println("Criando fundação com cimento, vergalhões de ferro e areia...");
    }


}
