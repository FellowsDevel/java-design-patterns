package com.fellows.patterns._03_behavioral._05_strategy;

public class CreditCardStrategy implements PaymentStrategy {

    private String name;
    private String cardNumber;
    private String cvv;
    private String dataExpiry;

    public CreditCardStrategy(String name, String cardNumber, String cvv, String dataExpiry) {
        this.name = name;
        this.cardNumber = cardNumber;
        this.cvv = cvv;
        this.dataExpiry = dataExpiry;
    }

    @Override
    public void pay(int amount) {
        System.out.println(amount + " pago com cartão de crédito/débito");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getCvv() {
        return cvv;
    }

    public void setCvv(String cvv) {
        this.cvv = cvv;
    }

    public String getDataExpiry() {
        return dataExpiry;
    }

    public void setDataExpiry(String dataExpiry) {
        this.dataExpiry = dataExpiry;
    }


}
