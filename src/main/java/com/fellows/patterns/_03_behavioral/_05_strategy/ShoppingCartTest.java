package com.fellows.patterns._03_behavioral._05_strategy;

public class ShoppingCartTest {

    public static void main(String[] args) {
        ShoppingCart shoppingCart = new ShoppingCart();

        Item i1 = new Item("1234", 10);
        Item i2 = new Item("4567", 40);

        shoppingCart.addItem(i1);
        shoppingCart.addItem(i2);

        shoppingCart.pay(new PaypalStrategy("myemail@example.com", "passw0rd"));

        shoppingCart.pay(new CreditCardStrategy("CARD NAME", "123445677896", "123", "12/22"));

    }
}
