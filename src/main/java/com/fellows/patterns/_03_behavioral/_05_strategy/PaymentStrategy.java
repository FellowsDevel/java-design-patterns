package com.fellows.patterns._03_behavioral._05_strategy;

public interface PaymentStrategy {
    void pay(int amount);
}
