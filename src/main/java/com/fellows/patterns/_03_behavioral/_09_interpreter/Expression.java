package com.fellows.patterns._03_behavioral._09_interpreter;

public interface Expression {
    String interpret(InterpreterContext ic);
}