package com.fellows.patterns._03_behavioral._09_interpreter;

public class IntToHexExpression implements Expression {

    private final int i;

    public IntToHexExpression(int i) {
        this.i = i;
    }

    @Override
    public String interpret(InterpreterContext ic) {
        return ic.getHexadecimalFormat(this.i);
    }
}
