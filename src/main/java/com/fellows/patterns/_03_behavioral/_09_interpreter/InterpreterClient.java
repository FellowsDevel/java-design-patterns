package com.fellows.patterns._03_behavioral._09_interpreter;

public class InterpreterClient {

    public InterpreterContext ic;

    public InterpreterClient(InterpreterContext ic) {
        this.ic = ic;
    }

    public static void main(String[] args) {
        String s1 = "28 in Binary";
        String s2 = "28 in Hexadecimal";

        InterpreterClient ic = new InterpreterClient(new InterpreterContext());
        System.out.println(s1 + " = " + ic.interpret(s1));
        System.out.println(s2 + " = " + ic.interpret(s2));
    }

    public String interpret(String str) {
        Expression exp;
        int i = Integer.parseInt(str.substring(0, str.indexOf(" ")));
        if (str.toLowerCase().contains("hexadecimal")) {
            exp = new IntToHexExpression(i);
        } else if (str.toLowerCase().contains("binary")) {
            exp = new IntToBinaryExpression(i);
        } else {
            return str;
        }
        return exp.interpret(ic);
    }
}
