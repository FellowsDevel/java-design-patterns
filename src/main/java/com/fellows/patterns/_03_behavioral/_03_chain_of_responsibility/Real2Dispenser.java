package com.fellows.patterns._03_behavioral._03_chain_of_responsibility;

public class Real2Dispenser extends Dispenser {

    public Real2Dispenser(int quantity) throws Exception {
        super(2, quantity);
    }

}
