package com.fellows.patterns._03_behavioral._03_chain_of_responsibility;

public abstract class Dispenser implements DispenseChain {

    private final int value;
    private DispenseChain chain;
    private int available;

    public Dispenser(int value, int available) throws Exception {
        if (value <= 0) {
            throw new Exception("Valor deve ser positivo");
        }
        this.value = value;
        this.available = available;
    }

    @Override
    public void dispense(Currency currency) {
        if (currency != null && currency.amount() >= this.value && this.available > 0) {

            int quantity = currency.amount() / this.value;
            int remainder = currency.amount() % this.value;

            if (quantity > this.available) {
                System.out.printf("1 - Dispensando: '%d' notas de %d\t= %3d%n", this.available, this.value,
                        this.available * this.value);
                Currency newValue = new Currency(currency.amount() - (this.available * this.value));
                this.available = 0;
                this.chain.dispense(newValue);
            } else {
                System.out.printf("2 - Dispensando: '%d' notas de %d\t= %3d%n", quantity, this.value,
                        quantity * this.value);
                this.available -= quantity;
                if (remainder != 0) {
                    this.chain.dispense(new Currency(remainder));
                }
            }

        } else {
            this.chain.dispense(currency);
        }
    }

    @Override
    public void setNextChain(DispenseChain nextChain) {
        this.chain = nextChain;
    }
}
