package com.fellows.patterns._03_behavioral._03_chain_of_responsibility;

public class Real100Dispenser extends Dispenser {

    public Real100Dispenser(int quantity) throws Exception {
        super(100, quantity);
    }
}
