package com.fellows.patterns._03_behavioral._03_chain_of_responsibility;

public class Real20Dispenser extends Dispenser {

    public Real20Dispenser(int quantity) throws Exception {
        super(20, quantity);
    }

}
