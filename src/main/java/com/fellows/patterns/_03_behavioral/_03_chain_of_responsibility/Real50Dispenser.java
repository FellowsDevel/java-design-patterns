package com.fellows.patterns._03_behavioral._03_chain_of_responsibility;

public class Real50Dispenser extends Dispenser {

    public Real50Dispenser(int quantity) throws Exception {
        super(50, quantity);
    }

}
