package com.fellows.patterns._03_behavioral._03_chain_of_responsibility;

public class Real1Dispenser extends Dispenser {

    public Real1Dispenser(int quantity) throws Exception {
        super(1, quantity);
    }

}
