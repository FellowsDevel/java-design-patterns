package com.fellows.patterns._03_behavioral._03_chain_of_responsibility;

import java.util.Scanner;

public class CaixaEletronicoDispensechain {
    private final DispenseChain dispenser;

    public CaixaEletronicoDispensechain() throws Exception {
        dispenser = new Real100Dispenser(3);
        DispenseChain chain50 = new Real50Dispenser(10);
        DispenseChain chain20 = new Real20Dispenser(22);
        DispenseChain chain10 = new Real10Dispenser(12);
        DispenseChain chain5 = new Real5Dispenser(20);
        DispenseChain chain2 = new Real2Dispenser(52);
        DispenseChain chain1 = new Real1Dispenser(123);
        dispenser.setNextChain(chain50);
        chain50.setNextChain(chain20);
        chain20.setNextChain(chain10);
        chain10.setNextChain(chain5);
        chain5.setNextChain(chain2);
        chain2.setNextChain(chain1);
    }

    public static void main(String[] args) throws Exception {
        CaixaEletronicoDispensechain caixa = new CaixaEletronicoDispensechain();
        int quantia = 0;
        System.out.println("Informe a quantia: ");
        Scanner input = new Scanner(System.in);
        quantia = input.nextInt();
        input.close();
        caixa.dispenser.dispense(new Currency(quantia));
    }
}
