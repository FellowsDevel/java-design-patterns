package com.fellows.patterns._03_behavioral._03_chain_of_responsibility;

public class Real5Dispenser extends Dispenser {

    public Real5Dispenser(int quantity) throws Exception {
        super(5, quantity);
    }

}
