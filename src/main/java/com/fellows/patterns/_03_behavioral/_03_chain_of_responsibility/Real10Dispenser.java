package com.fellows.patterns._03_behavioral._03_chain_of_responsibility;

public class Real10Dispenser extends Dispenser {

    public Real10Dispenser(int quantity) throws Exception {
        super(10, quantity);
    }

}
