package com.fellows.patterns._03_behavioral._03_chain_of_responsibility;

public interface DispenseChain {

    void setNextChain(DispenseChain nextChain);

    void dispense(Currency currency);
}
