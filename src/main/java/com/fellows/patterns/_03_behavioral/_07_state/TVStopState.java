package com.fellows.patterns._03_behavioral._07_state;

public class TVStopState implements State {

    @Override
    public void doAction() {
        System.out.println("A TV foi DESLIGADA");
    }
}
