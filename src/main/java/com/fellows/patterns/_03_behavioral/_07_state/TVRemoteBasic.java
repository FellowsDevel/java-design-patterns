package com.fellows.patterns._03_behavioral._07_state;

public class TVRemoteBasic {

    private String state = "";

    public static void main(String[] args) {
        TVRemoteBasic remoteBasic = new TVRemoteBasic();
        remoteBasic.setState("ON");
        remoteBasic.doAction();

        remoteBasic.setState("OFF");
        remoteBasic.doAction();
    }

    public void setState(String state) {
        this.state = state;
    }

    public void doAction() {
        if (state.equalsIgnoreCase("ON")) {
            System.out.println("TV está LIGADA");
        } else {
            System.out.println("TV está DESLIGADA");
        }
    }
}
