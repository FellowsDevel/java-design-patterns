package com.fellows.patterns._03_behavioral._07_state;

public class TVContext implements State {

    private State tvState;

    public State getState() {
        return tvState;
    }

    public void setState(State tvState) {
        this.tvState = tvState;
    }

    @Override
    public void doAction() {
        this.tvState.doAction();
    }
}
