package com.fellows.patterns._03_behavioral._07_state;

public interface State {
    void doAction();
}
