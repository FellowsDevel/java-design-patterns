package com.fellows.patterns._03_behavioral._04_observer;

import java.util.ArrayList;
import java.util.List;

public class MyTopic implements Subject {


    private final Object MUTEX = new Object();
    private final List<Observer> observers;
    private String message;
    private boolean changed;

    public MyTopic() {
        this.observers = new ArrayList<>();
    }

    @Override
    public void register(Observer observer) {
        if (observer == null) {
            throw new NullPointerException("Observador nulo");
        }
        synchronized (MUTEX) {
            if (!observers.contains(observer)) {
                observers.add(observer);
            }
        }
    }

    @Override
    public void unregister(Observer observer) {
        synchronized (MUTEX) {
            observers.remove(observer);
        }
    }

    @Override
    public void notifyObservers() {
        List<Observer> local = null;
        synchronized (MUTEX) {
            if (!changed) {
                return;
            }
            local = new ArrayList<>(this.observers);
            this.changed = false;
        }
        for (Observer o : local) {
            o.update();
        }
    }

    @Override
    public Object getUpdate(Observer observer) {
        return this.message;
    }

    public void postMessage(String msg) {
        System.out.println("Mensagem postada: " + msg);
        this.message = msg;
        this.changed = true;
        notifyObservers();
    }
}
