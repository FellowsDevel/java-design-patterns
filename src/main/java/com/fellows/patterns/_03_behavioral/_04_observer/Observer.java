package com.fellows.patterns._03_behavioral._04_observer;

public interface Observer {

    void update();

    void setSubject(Subject subject);
}
