package com.fellows.patterns._03_behavioral._04_observer;

public class MyTopicSubscriber implements Observer {

    private final String name;
    private Subject subject;

    public MyTopicSubscriber(String name) {
        this.name = name;
    }

    @Override
    public void update() {
        String msg = (String) subject.getUpdate(this);
        if (msg == null) {
            System.out.println(name + ":: Sem novas mensgens");
        } else {
            System.out.println(name + ":: Consumindo mensagem: " + msg);
        }
    }

    @Override
    public void setSubject(Subject subject) {
        this.subject = subject;
    }
}
