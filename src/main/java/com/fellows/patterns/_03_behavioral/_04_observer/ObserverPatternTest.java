package com.fellows.patterns._03_behavioral._04_observer;

public class ObserverPatternTest {

    public static void main(String[] args) {
        MyTopic topic = new MyTopic();

        Observer ob1 = new MyTopicSubscriber("Obs1");
        Observer ob2 = new MyTopicSubscriber("Obs2");
        Observer ob3 = new MyTopicSubscriber("Obs3");

        topic.register(ob1);
        topic.register(ob2);
        topic.register(ob3);

        ob1.setSubject(topic);
        ob2.setSubject(topic);
        ob3.setSubject(topic);

        ob1.update();
        ob3.update();

        topic.postMessage("Nova mensagem");
        ob2.update();
        topic.unregister(ob2);

        topic.postMessage("Nova mensagem");
    }
}
