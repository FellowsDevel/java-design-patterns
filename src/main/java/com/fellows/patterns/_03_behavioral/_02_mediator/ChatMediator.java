package com.fellows.patterns._03_behavioral._02_mediator;

public interface ChatMediator {

    void sendMessage(String msg, User user);

    void addUser(User user);

}
