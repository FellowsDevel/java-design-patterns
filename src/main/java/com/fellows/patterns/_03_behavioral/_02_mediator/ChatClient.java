package com.fellows.patterns._03_behavioral._02_mediator;

public class ChatClient {

    public static void main(String[] args) {
        ChatMediator mediator = new ChatMediatorImpl();
        User user1 = new UserImpl(mediator, "André");
        User user2 = new UserImpl(mediator, "Ana");
        User user3 = new UserImpl(mediator, "Fernanda");
        User user4 = new UserImpl(mediator, "Vitor");

        mediator.addUser(user1);
        mediator.addUser(user2);
        mediator.addUser(user3);
        mediator.addUser(user4);

        user1.send("Olá");

    }
}
