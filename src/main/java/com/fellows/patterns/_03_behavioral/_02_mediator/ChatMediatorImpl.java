package com.fellows.patterns._03_behavioral._02_mediator;

import java.util.ArrayList;
import java.util.List;

public class ChatMediatorImpl implements ChatMediator {

    private final List<User> users;

    public ChatMediatorImpl() {
        this.users = new ArrayList<>();
    }


    @Override
    public void sendMessage(String msg, User user) {
        for (User u : this.users) {
            // A mensagem não deve ser recebida pelo próprio usuário
            if (u != user) {
                u.receive(msg);
            }
        }
    }

    @Override
    public void addUser(User user) {
        this.users.add(user);
    }
}
