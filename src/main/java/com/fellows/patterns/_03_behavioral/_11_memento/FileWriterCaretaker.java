package com.fellows.patterns._03_behavioral._11_memento;

public class FileWriterCaretaker {

    private Object obj;

    public void save(FileWriteUtil fileWriteUtil) {
        this.obj = fileWriteUtil.save();
    }

    public void undo(FileWriteUtil fileWriteUtil) {
        fileWriteUtil.undoToLastSave(obj);
    }
}
