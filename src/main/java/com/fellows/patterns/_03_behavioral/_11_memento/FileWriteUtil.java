package com.fellows.patterns._03_behavioral._11_memento;

public class FileWriteUtil {

    private String fileName;
    private StringBuilder content;

    public FileWriteUtil(String fileName) {
        this.fileName = fileName;
        this.content = new StringBuilder();
    }

    @Override
    public String toString() {
        return this.content.toString();
    }

    public void write(String str) {
        content.append(str);
    }

    public Memento save() {
        return new Memento(this.fileName, this.content);
    }

    public void undoToLastSave(Object obj) {
        Memento memento = (Memento) obj;
        this.fileName = memento.fileName;
        this.content = memento.content;
    }

    private record Memento(String fileName, StringBuilder content) {
        private Memento(String fileName, StringBuilder content) {
            this.fileName = fileName;
            // observe o Deep Copy para que não referenciem o mesmo objeto
            this.content = new StringBuilder(content);
        }
    }
}
