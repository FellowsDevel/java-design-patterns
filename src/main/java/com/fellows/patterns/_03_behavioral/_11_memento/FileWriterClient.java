package com.fellows.patterns._03_behavioral._11_memento;

public class FileWriterClient {

    public static void main(String[] args) {
        FileWriterCaretaker caretaker = new FileWriterCaretaker();
        FileWriteUtil filewriter = new FileWriteUtil("data.txt");
        filewriter.write("Primeira mensagem\n");
        System.out.println(filewriter + "\n\n");

        caretaker.save(filewriter);

        filewriter.write("Segunda mensagem\n");
        System.out.println(filewriter + "\n\n");

        caretaker.undo(filewriter);

        System.out.println(filewriter + "\n\n");
    }
}
