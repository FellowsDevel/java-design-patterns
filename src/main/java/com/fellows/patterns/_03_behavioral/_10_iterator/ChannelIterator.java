package com.fellows.patterns._03_behavioral._10_iterator;

public interface ChannelIterator {

    boolean hasNext();

    Channel next();
}
