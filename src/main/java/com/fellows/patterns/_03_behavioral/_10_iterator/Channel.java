package com.fellows.patterns._03_behavioral._10_iterator;

public record Channel(double frequency, ChannelTypeEnum TYPE) {

    @Override
    public String toString() {
        return "Frequency= " + frequency() +
                ", TYPE= " + TYPE();
    }
}
