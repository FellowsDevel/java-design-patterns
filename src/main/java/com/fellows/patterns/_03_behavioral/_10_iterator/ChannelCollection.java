package com.fellows.patterns._03_behavioral._10_iterator;

public interface ChannelCollection {

    void addChannel(Channel channel);

    void removeChannel(Channel channel);

    ChannelIterator iterator(ChannelTypeEnum type);
}
