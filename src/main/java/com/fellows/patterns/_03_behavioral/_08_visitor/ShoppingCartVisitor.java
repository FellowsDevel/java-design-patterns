package com.fellows.patterns._03_behavioral._08_visitor;

public interface ShoppingCartVisitor {
    int visit(Book book);

    int visit(Fruit fruit);
}
