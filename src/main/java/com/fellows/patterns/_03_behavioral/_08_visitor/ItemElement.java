package com.fellows.patterns._03_behavioral._08_visitor;

public interface ItemElement {
    int accept(ShoppingCartVisitor visitor);
}
