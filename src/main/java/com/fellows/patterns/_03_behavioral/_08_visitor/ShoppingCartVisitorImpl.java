package com.fellows.patterns._03_behavioral._08_visitor;

public class ShoppingCartVisitorImpl implements ShoppingCartVisitor {


    @Override
    public int visit(Book book) {
        int cost = 0;
        if (book.price() > 50) {
            cost = book.price() - 5;
        } else {
            cost = book.price();
        }
        System.out.println("Livro ISBN:" + book.isbn() + " custo: " + cost);
        return cost;
    }

    @Override
    public int visit(Fruit fruit) {
        int cost = fruit.pricePerKg() * fruit.weight();
        System.out.println(fruit.name() + " custa: " + cost);
        return cost;
    }
}
