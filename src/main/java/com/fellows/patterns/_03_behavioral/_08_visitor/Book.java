package com.fellows.patterns._03_behavioral._08_visitor;

public record Book(int price, String isbn) implements ItemElement {

    @Override
    public int accept(ShoppingCartVisitor visitor) {
        return visitor.visit(this);
    }
}
