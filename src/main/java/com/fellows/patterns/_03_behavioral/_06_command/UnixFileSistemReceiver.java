package com.fellows.patterns._03_behavioral._06_command;

public class UnixFileSistemReceiver implements FileSystemReceiver {

    @Override
    public void openFile() {
        System.out.println("Abrido arquivo no Unix");
    }

    @Override
    public void writeFile() {
        System.out.println("Escrevendo arquivo no Unix");
    }

    @Override
    public void closeFile() {
        System.out.println("Fechando arquivo no Unix");
    }
}
