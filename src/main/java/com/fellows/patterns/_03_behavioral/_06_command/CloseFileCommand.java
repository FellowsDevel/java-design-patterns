package com.fellows.patterns._03_behavioral._06_command;

public class CloseFileCommand implements Command {

    private final FileSystemReceiver receiver;

    public CloseFileCommand(FileSystemReceiver receiver) {
        this.receiver = receiver;
    }

    @Override
    public void execute() {
        this.receiver.closeFile();
    }
}
