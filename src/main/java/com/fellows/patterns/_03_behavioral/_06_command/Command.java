package com.fellows.patterns._03_behavioral._06_command;

public interface Command {
    void execute();
}
