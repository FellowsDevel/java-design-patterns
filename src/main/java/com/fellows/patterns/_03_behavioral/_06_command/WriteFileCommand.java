package com.fellows.patterns._03_behavioral._06_command;

public class WriteFileCommand implements Command {

    private final FileSystemReceiver receiver;

    public WriteFileCommand(FileSystemReceiver receiver) {
        this.receiver = receiver;
    }

    @Override
    public void execute() {
        this.receiver.writeFile();
    }
}
