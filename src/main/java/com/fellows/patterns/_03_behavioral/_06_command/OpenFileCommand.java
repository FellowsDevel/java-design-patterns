package com.fellows.patterns._03_behavioral._06_command;

public class OpenFileCommand implements Command {

    private final FileSystemReceiver receiver;

    public OpenFileCommand(FileSystemReceiver receiver) {
        this.receiver = receiver;
    }

    @Override
    public void execute() {
        this.receiver.openFile();
    }
}
