package com.fellows.patterns._03_behavioral._06_command;

public interface FileSystemReceiver {

    void openFile();

    void writeFile();

    void closeFile();

}
