package com.fellows.patterns._03_behavioral._06_command;

public class WindowsFileSistemReceiver implements FileSystemReceiver {

    @Override
    public void openFile() {
        System.out.println("Abrido arquivo no Windows");
    }

    @Override
    public void writeFile() {
        System.out.println("Escrevendo arquivo no Windows");
    }

    @Override
    public void closeFile() {
        System.out.println("Fechando arquivo no Windows");
    }
}
