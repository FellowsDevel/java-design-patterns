package com.fellows.patterns._03_behavioral._06_command;

public class FileSystemReceiverUtil {

    public static FileSystemReceiver getUnderyingFileSystem() {
        String osName = System.getProperty("os.name");
        System.out.println("Sistema operacional: " + osName);
        if (osName.contains("Windows")) {
            return new WindowsFileSistemReceiver();
        } else {
            return new UnixFileSistemReceiver();
        }
    }
}
