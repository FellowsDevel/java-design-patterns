package com.fellows.patterns._02_structural._03_proxy;

public class CommandExecutorImpl implements CommandExecutor {

    @Override
    public void runCommand(String command) throws Exception {
        Runtime.getRuntime().exec(command);
        System.out.println("Comando '" + command + "' executado.");
    }
}
