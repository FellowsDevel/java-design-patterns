package com.fellows.patterns._02_structural._03_proxy;

public class CommandExecutorProxy implements CommandExecutor {

    private boolean isAdmin;
    private final CommandExecutor executor;

    public CommandExecutorProxy(String user, String pwd) {
        if ("Admin".equals(user) && "@rr0w".equals(pwd)) {
            isAdmin = true;
        }
        executor = new CommandExecutorImpl();
    }

    @Override
    public void runCommand(String command) throws Exception {
        if (isAdmin) {
            executor.runCommand(command);
        } else {
            if (command.trim().toLowerCase().startsWith("rm")) {
                throw new Exception("O comando 'rm' não é permitido para usuários não admin.");
            } else {
                executor.runCommand(command);
            }
        }
    }
}
