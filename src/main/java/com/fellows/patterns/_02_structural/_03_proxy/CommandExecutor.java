package com.fellows.patterns._02_structural._03_proxy;

public interface CommandExecutor {
    void runCommand(String command) throws Exception;
}
