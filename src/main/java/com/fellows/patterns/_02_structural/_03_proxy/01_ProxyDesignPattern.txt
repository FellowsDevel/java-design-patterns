O padrão de design Proxy é um dos padrões de design estruturais (Structural Design Pattern) e é um dos
padrões mais simples de se entender.
De acordo com o GoF, a intenção do padrão é:
    - Prover um substituto ou um espaço reservado para outro objeto controlar o acesso a ele

A definição por si só é bem clara e o padrão proxy é utilizado quendo desejamos prover um acesso controlado a uma
funcionalidade. Digamos que temos uma classe que pode executar alguns comandos no sistema. Agora se somos nós que
usamos ela, tudo bem, mas se quisermos utilizar o programa numa aplicação cliente, pode acontecer diversos problemas
porque o programa cliente pode executar, por exemplo, comandos para excluir arquivos do sistema ou alterar
alguma configuração que não desejamos. Aqui a classe proxy pode ser criada para procer um acesso controlado ao programa.

A - Classe principal
    Como programamos Java em termos de interfaces, aqui está nossa interface e a classe que a implementa.
    - CommandExecutor.java
    - CommandExecutorImpl.java

B - Classe Proxy
    Agora queremos prover acesso irrestrito apenas para usuários ADMIN na classe CommandExecutor, se o usuário
    não é ADMIN então ele só poderá executar alguns comandos limitados.
    Esta é uma implementação bem simples de uma classe proxy.
    - CommandExecutorProxy.java

C - Programa de teste do padrão Proxy
    - ProxyPatternTest.java

OBS. A utilização mais comum para o padrão Proxy é de controlar o acesso ou prover uma implementação Wrapper
     para melhor performance