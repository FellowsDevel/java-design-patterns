package com.fellows.patterns._02_structural._07_decorator;

public class LuxuryCar extends CarDecorator {

    public LuxuryCar(Car car) {
        super(car);
    }

    @Override
    public void assemble() {
        car.assemble();
        System.out.print(" Adicionando funcionalidades do carro de Luxo.");
    }
}
