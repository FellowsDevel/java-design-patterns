package com.fellows.patterns._02_structural._07_decorator;

public class SportsCar extends CarDecorator {

    public SportsCar(Car car) {
        super(car);
    }

    @Override
    public void assemble() {
        car.assemble();
        System.out.print(" Adicionando funcionalidades do carro Esporte.");
    }
}
