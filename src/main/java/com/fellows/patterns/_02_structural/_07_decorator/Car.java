package com.fellows.patterns._02_structural._07_decorator;

public interface Car {
    void assemble();
}
