package com.fellows.patterns._02_structural._07_decorator;

public class BasicCar implements Car {
    @Override
    public void assemble() {
        System.out.print("Basic Car;");
    }
}
