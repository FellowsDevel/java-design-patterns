package com.fellows.patterns._02_structural._06_bridge;

public class Triangle extends Shape {

    public Triangle(Color color) {
        super(color);
    }

    @Override
    public void applyColor() {
        System.out.print("Triangulo preenchido com a cor: ");
        color.applyColor();
    }
}
