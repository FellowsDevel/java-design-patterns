package com.fellows.patterns._02_structural._06_bridge;

public class Pentagon extends Shape {

    public Pentagon(Color color) {
        super(color);
    }

    @Override
    public void applyColor() {
        System.out.print("Pentagono preenchido com a cor: ");
        color.applyColor();
    }
}
