package com.fellows.patterns._02_structural._06_bridge;

public class RedColor implements Color {

    @Override
    public void applyColor() {
        System.out.println("vermelho.");
    }
}
