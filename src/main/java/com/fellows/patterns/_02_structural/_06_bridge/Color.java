package com.fellows.patterns._02_structural._06_bridge;

public interface Color {
    void applyColor();
}
