package com.fellows.patterns._02_structural._06_bridge;

public class GreenColor implements Color {
    @Override
    public void applyColor() {
        System.out.println("verde.");
    }
}
