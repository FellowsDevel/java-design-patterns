package com.fellows.patterns._02_structural._04_flyweight;

import java.util.HashMap;

public class ShapeFactory {

    private static final HashMap<ShapeType, Shape> shapes = new HashMap<>();

    public static Shape getShape(ShapeType shapeType) {

        Shape shape = shapes.get(shapeType);

        if (shape == null) {
            if (shapeType.equals(ShapeType.OVAL_FILL)) {
                shape = new Oval(true);
            } else if (shapeType.equals(ShapeType.OVAL_NOFILL)) {
                shape = new Oval(false);
            } else if (shapeType.equals(ShapeType.LINE)) {
                shape = new Line();
            }
            shapes.put(shapeType, shape);
        }

        return shape;
    }

    /**
     * O Java Enum é para segurança na escolha do tipo, Java Composition (mapa shapes) e
     * Factory Pattern no método getShape
     */
    public enum ShapeType {
        OVAL_FILL, OVAL_NOFILL, LINE
    }
}
