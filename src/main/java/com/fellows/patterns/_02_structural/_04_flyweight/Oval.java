package com.fellows.patterns._02_structural._04_flyweight;

import java.awt.*;

public class Oval implements Shape {

    private final boolean fill;

    public Oval(boolean fill) {
        this.fill = fill;
        System.out.println("Criando objeto Oval com preenchimento = " + fill);
        // adicionando delay para parecer custoso
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void draw(Graphics circle, int x, int y, int width, int height, Color color) {
        circle.setColor(color);
        circle.drawOval(x, y, width, height);
        if (fill) {
            circle.fillOval(x, y, width, height);
        }
    }
}
