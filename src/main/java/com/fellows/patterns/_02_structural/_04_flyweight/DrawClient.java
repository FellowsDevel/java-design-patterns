package com.fellows.patterns._02_structural._04_flyweight;

import javax.swing.*;
import java.awt.*;
import java.io.Serial;
import java.util.concurrent.ThreadLocalRandom;

public class DrawClient extends JFrame {
    @Serial
    private static final long serialVersionUID = -123123123123432343L;
    private static final ShapeFactory.ShapeType[] shapes = {
            ShapeFactory.ShapeType.LINE,
            ShapeFactory.ShapeType.OVAL_FILL,
            ShapeFactory.ShapeType.OVAL_NOFILL
    };
    private static final Color[] colors = {Color.RED, Color.GREEN, Color.YELLOW};
    private final int WIDTH;
    private final int HEIGHT;

    private final JPanel panel = new JPanel();

    public DrawClient(int width, int height) {
        WIDTH = width;
        HEIGHT = height;

        Container container = getContentPane();
        JButton startButton = new JButton("Desenhar");
        JButton clearButton = new JButton("Apagar");

        container.add(panel, BorderLayout.CENTER);
        container.add(startButton, BorderLayout.SOUTH);
        container.add(clearButton, BorderLayout.EAST);

        setSize(WIDTH, HEIGHT);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);

        startButton.addActionListener(actionEvent -> {
            Graphics g = panel.getGraphics();
            for (int i = 0; i < 200; ++i) {
                Shape shape = ShapeFactory.getShape(getRandomShape());
                shape.draw(g, getRandomX(), getRandomY(), getRandomWidth(), getRandomHeight(), getRandomColor());
            }
        });

        clearButton.addActionListener(actionEvent -> {
            panel.getGraphics().create(0, 0, WIDTH, HEIGHT);
            panel.updateUI();
        });

    }

    public static void main(String[] args) {
        new DrawClient(800, 600);
    }

    private ShapeFactory.ShapeType getRandomShape() {
        return shapes[genRand(0, shapes.length - 1)];
    }

    private int genRand(int min, int max) {
        return ThreadLocalRandom.current().nextInt(min, max + 1);
    }

    private int getRandomX() {
        int rnd = genRand(1, WIDTH);
        System.out.printf("min: %5d\tmax: %5d\tgetRandomX        : %5d %n", 1, WIDTH, rnd);
        return rnd;
    }

    private int getRandomY() {
        int rnd = genRand(1, HEIGHT);
        System.out.printf("min: %5d\tmax: %5d\tgetRandomY        : %5d %n", 1, HEIGHT, rnd);
        return rnd;
    }

    private int getRandomWidth() {
        int rnd = genRand(1, WIDTH);
        System.out.printf("min: %5d\tmax: %5d\tgetRandomWidth    : %5d %n", 1, WIDTH, rnd);
        return rnd;
    }

    private int getRandomHeight() {
        int rnd = genRand(1, HEIGHT);
        System.out.printf("min: %5d\tmax: %5d\tgetRandomHeight   : %5d %n", 1, HEIGHT, rnd);
        return rnd;
    }

    private Color getRandomColor() {
        int rnd = genRand(0, colors.length - 1);
        System.out.printf("min: %5d\tmax: %5d\tgetRandomColor    : %5d %n", 0, colors.length, rnd);
        return colors[rnd];
    }

}