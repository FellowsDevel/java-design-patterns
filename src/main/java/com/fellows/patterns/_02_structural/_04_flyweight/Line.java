package com.fellows.patterns._02_structural._04_flyweight;

import java.awt.*;

public class Line implements Shape {

    public Line() {
        System.out.println("Criando objeto Line");
        // adicionando delay para parecer custoso
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void draw(Graphics line, int x, int y, int x2, int y2, Color color) {
        line.setColor(color);
        line.drawLine(x, y, x2, y2);
    }
}
