package com.fellows.patterns._02_structural._01_adapter;

public class SocketObjectAdapterImpl implements SocketAdapter {

    private final Socket socket = new Socket();

    @Override
    public Volt get127Volt() {
        return socket.getVolt();
    }

    @Override
    public Volt get12Volt() {
        Volt v = socket.getVolt();
        return convertVolt(v, 10);
    }

    @Override
    public Volt get3Volt() {
        Volt v = socket.getVolt();
        return convertVolt(v, 40);
    }

    private Volt convertVolt(Volt volt, int i) {
        return new Volt(volt.getVolts() / i);
    }
}
