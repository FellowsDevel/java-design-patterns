package com.fellows.patterns._02_structural._01_adapter;

public interface SocketAdapter {

    Volt get127Volt();

    Volt get12Volt();

    Volt get3Volt();

}
