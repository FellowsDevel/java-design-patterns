package com.fellows.patterns._02_structural._01_adapter;

public class Socket {
    public Volt getVolt() {
        return new Volt(127);
    }
}
