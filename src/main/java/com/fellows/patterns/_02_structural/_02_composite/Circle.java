package com.fellows.patterns._02_structural._02_composite;

public class Circle implements Shape {

    @Override
    public void draw(String color) {
        System.out.println("Desenhando um círculo com a cor " + color);
    }
}
