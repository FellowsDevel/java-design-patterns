package com.fellows.patterns._02_structural._02_composite;

public interface Shape {
    void draw(String color);
}
