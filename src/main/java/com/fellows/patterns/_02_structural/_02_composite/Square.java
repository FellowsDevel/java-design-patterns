package com.fellows.patterns._02_structural._02_composite;

public class Square implements Shape {

    @Override
    public void draw(String color) {
        System.out.println("Desenhando um quadrado com a cor " + color);
    }
}
