package com.fellows.patterns._02_structural._02_composite;

import java.util.ArrayList;
import java.util.List;

public class Drawing implements Shape {

    private final List<Shape> shapes = new ArrayList<>();

    @Override
    public void draw(String color) {
        for (Shape shape : shapes) {
            shape.draw(color);
        }
    }

    public void add(Shape shape) {
        shapes.add(shape);
    }

    public void remove(Shape shape) {
        shapes.remove(shape);
    }

    public void clear() {
        System.out.println("Limpando todos os objetos do desenho");
        shapes.clear();
    }
}
