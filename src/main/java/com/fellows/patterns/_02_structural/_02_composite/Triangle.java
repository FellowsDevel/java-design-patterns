package com.fellows.patterns._02_structural._02_composite;

public class Triangle implements Shape {

    @Override
    public void draw(String color) {
        System.out.println("Desenhando um triangulo com a cor " + color);
    }
}
