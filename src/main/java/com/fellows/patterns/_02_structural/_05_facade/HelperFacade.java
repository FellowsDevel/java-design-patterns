package com.fellows.patterns._02_structural._05_facade;

import java.sql.Connection;

public class HelperFacade {

    public static void generateReport(DBType dbType, ReportType reportType, String tableName) {

        switch (dbType) {
            case MYSQL -> generateMySqlReport(reportType, tableName);
            case ORACLE -> generateOracleReport(reportType, tableName);
        }
    }

    private static void generateOracleReport(ReportType reportType, String tableName) {
        Connection connection;
        connection = OracleHelper.getOracleDBConnection();
        OracleHelper oracleHelper = new OracleHelper();
        switch (reportType) {
            case HTML -> oracleHelper.generateoracleHTMLReport(tableName, connection);
            case PDF -> oracleHelper.generateOraclePDFReport(tableName, connection);
        }
    }

    private static void generateMySqlReport(ReportType reportType, String tableName) {
        Connection connection;
        connection = MySqlHelper.getMySqlDBConnection();
        MySqlHelper mySqlHelper = new MySqlHelper();
        switch (reportType) {
            case HTML -> mySqlHelper.generateMySqlHTMLReport(tableName, connection);
            case PDF -> mySqlHelper.generateMySqlPDFReport(tableName, connection);
        }
    }

    public enum DBType {
        MYSQL, ORACLE
    }

    public enum ReportType {
        HTML, PDF
    }
}
