package com.fellows.patterns._02_structural._05_facade;

import java.sql.Connection;

public class OracleHelper {

    public static Connection getOracleDBConnection() {
        return null;
    }

    public void generateOraclePDFReport(String tableName, Connection connection) {
        // processo de gerar o relatório em PDF
    }

    public void generateoracleHTMLReport(String tableName, Connection connection) {
        // processo de gerar o relatório em HTML
    }
}
