package com.fellows.patterns._02_structural._05_facade;

import java.sql.Connection;

public class MySqlHelper {

    public static Connection getMySqlDBConnection() {
        return null;
    }

    public void generateMySqlPDFReport(String tableName, Connection connection) {
        // processo de gerar o relatório em PDF
    }

    public void generateMySqlHTMLReport(String tableName, Connection connection) {
        // processo de gerar o relatório em HTML
    }
}
