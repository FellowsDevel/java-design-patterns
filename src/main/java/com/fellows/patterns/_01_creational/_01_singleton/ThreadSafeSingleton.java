package com.fellows.patterns._01_creational._01_singleton;

public class ThreadSafeSingleton {
    private static ThreadSafeSingleton instance;

    private ThreadSafeSingleton() {
    }

    /**
     * Este método abaixo funciona bem e provê segurança nas threads (tread-safety) nas reduz a performance
     * por causa do custo associado ao método synchronized, no entanto nós precisamos disso somente nas
     * primeiras poucas threads que possam criar as instâncias separadas.
     *
     * @return
     */
    public static synchronized ThreadSafeSingleton getInstance() {
        if (instance == null) {
            instance = new ThreadSafeSingleton();
        }
        return instance;
    }

    /**
     * Para evitar o problema acima, utiliza-se o princípio Double Checked Locking.
     * Nesta aproximação, o bloco sincronizado utilizado é utilizado com uma verificação adicional
     * para assegurar-se que apenas uma instancia do Singleton será criada.
     * Abaixo segue o exemplo
     */
    public static ThreadSafeSingleton getInstanceDoubleLocking() {
        if (instance == null) {
            synchronized (ThreadSafeSingleton.class) {
                if (instance == null) {
                    instance = new ThreadSafeSingleton();
                }
            }
        }
        return instance;
    }
}

