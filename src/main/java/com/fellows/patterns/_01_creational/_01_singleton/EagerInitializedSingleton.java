package com.fellows.patterns._01_creational._01_singleton;

public class EagerInitializedSingleton implements Singleton {

    private static final long serialVersionUID = 2006268853413859779L;

    private static final EagerInitializedSingleton instancia = new EagerInitializedSingleton();

    //O construtor privado é para evitar que a aplicação cliente utilize o construtor da classe.
    private EagerInitializedSingleton() {
        System.out.println("Criando singleton...");
    }

    public static EagerInitializedSingleton getInstance() {
        return instancia;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    /**
     * O método abaixo resolve o problema da serialização;Deserialização
     * @return
     */
//    protected Object readResolve(){
//        return getInstance();
//    }
}
