package com.fellows.patterns._01_creational._01_singleton;

public class LazyInitializationSingleton implements Singleton {

    private static final long serialVersionUID = -5642464129236331642L;

    private static LazyInitializationSingleton instance;

    private LazyInitializationSingleton() {
    }

    public static LazyInitializationSingleton getInstance() {
        if (instance == null) {
            instance = new LazyInitializationSingleton();
        }
        return instance;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
