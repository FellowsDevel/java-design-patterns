package com.fellows.patterns._01_creational._01_singleton;

public class BillPughSingleton implements Singleton {

    private static final long serialVersionUID = 2040148125649390955L;

    private BillPughSingleton() {
    }

    public static BillPughSingleton getInstance() {
        return SingletonHelper.INSTANCE;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    private static class SingletonHelper {
        private static final BillPughSingleton INSTANCE = new BillPughSingleton();
    }
}
