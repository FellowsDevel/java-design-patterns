package com.fellows.patterns._01_creational._01_singleton;

import java.io.Serializable;

public interface Singleton extends Serializable, Cloneable {

}
