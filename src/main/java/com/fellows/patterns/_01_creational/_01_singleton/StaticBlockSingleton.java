package com.fellows.patterns._01_creational._01_singleton;

public class StaticBlockSingleton implements Singleton {


    private static final long serialVersionUID = 1630094917799029706L;

    private static final StaticBlockSingleton instance;

    static {
        try {
            instance = new StaticBlockSingleton();
        } catch (Exception e) {
            throw new RuntimeException("Ocorreu uma exceção criando a instância do Singleton");
        }
    }

    private StaticBlockSingleton() {
    }

    public static StaticBlockSingleton getInstance() {
        return instance;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}

