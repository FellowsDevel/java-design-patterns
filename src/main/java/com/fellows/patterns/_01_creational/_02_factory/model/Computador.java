package com.fellows.patterns._01_creational._02_factory.model;

public abstract class Computador {

    public abstract String getRAM();

    public abstract String getHDD();

    public abstract String getCPU();

    @Override
    public String toString() {
        return "Ram: " + this.getRAM() + "\n" +
                "Hdd: " + this.getHDD() + "\n" +
                "Cpu: " + this.getCPU() + "\n";
    }
}
