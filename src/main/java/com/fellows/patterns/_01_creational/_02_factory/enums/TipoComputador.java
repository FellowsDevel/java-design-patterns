package com.fellows.patterns._01_creational._02_factory.enums;

public enum TipoComputador {
    SERVIDOR,
    PC
}
