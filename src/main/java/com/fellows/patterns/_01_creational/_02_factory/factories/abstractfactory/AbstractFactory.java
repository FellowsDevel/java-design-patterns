package com.fellows.patterns._01_creational._02_factory.factories.abstractfactory;

import com.fellows.patterns._01_creational._02_factory.model.Computador;

public class AbstractFactory {

    public static Computador getComputador(ComputadorAbstractFactory factory) {
        return factory.createComputador();
    }
}
