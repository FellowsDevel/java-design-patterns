package com.fellows.patterns._01_creational._02_factory.factories.abstractfactory;

import com.fellows.patterns._01_creational._02_factory.model.Computador;
import com.fellows.patterns._01_creational._02_factory.model.Pc;

public class PcFactory implements ComputadorAbstractFactory {

    private final String ram;
    private final String hdd;
    private final String cpu;

    public PcFactory(String ram, String hdd, String cpu) {
        this.ram = ram;
        this.hdd = hdd;
        this.cpu = cpu;
    }

    @Override
    public Computador createComputador() {
        return new Pc(ram, hdd, cpu);
    }
}
