package com.fellows.patterns._01_creational._02_factory.factories.abstractfactory;

import com.fellows.patterns._01_creational._02_factory.model.Computador;
import com.fellows.patterns._01_creational._02_factory.model.Servidor;

public class ServidorFactory implements ComputadorAbstractFactory {

    private final String ram;
    private final String hdd;
    private final String cpu;

    public ServidorFactory(String ram, String hdd, String cpu) {
        this.ram = ram;
        this.hdd = hdd;
        this.cpu = cpu;
    }

    @Override
    public Computador createComputador() {
        return new Servidor(ram, hdd, cpu);
    }
}
