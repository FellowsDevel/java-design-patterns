package com.fellows.patterns._01_creational._02_factory.factories.normalfactory;

import com.fellows.patterns._01_creational._02_factory.enums.TipoComputador;
import com.fellows.patterns._01_creational._02_factory.model.Computador;
import com.fellows.patterns._01_creational._02_factory.model.Pc;
import com.fellows.patterns._01_creational._02_factory.model.Servidor;

public class ComputadorFactory {

    public static Computador getComputador(TipoComputador tipo, String ram, String hdd, String cpu) {

        return switch (tipo) {
            case SERVIDOR -> new Servidor(ram, hdd, cpu);
            case PC -> new Pc(ram, hdd, cpu);
            default -> null;
        };

    }
}
