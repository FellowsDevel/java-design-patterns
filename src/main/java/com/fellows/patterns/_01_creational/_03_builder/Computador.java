package com.fellows.patterns._01_creational._03_builder;

/**
 * 1 - Antes de mais nada, temos que criar uma classe interna estatica para então copiar todos os argumentos
 * da classe externa para a classe Builder. Temos que seguir a convenção de nomenclatura e se a classe
 * se chama Computador, então a classe interna estatica se chamará ComputadorBuilder.
 * 2 - A classe builder deve possuir uma construtor público com todos os atributos necessários como parametros.
 * 3 - A classe builder deve conter métodos para setar os parametros opcionais retornando o mesmo Objeto Builder
 * após setar o atributo opcional.
 * 4 - O passo final é prover um método build() na classe builder que retorne o Objeto solicitado pelo programa
 * cliente. Para isto devemos ter um construtor privado na classe recebendo a classe Builder como argumento.
 */
public class Computador {

    // Atributos obrigatórios
    private final String HDD;
    private final String RAM;

    // Atributos opcionais
    private final boolean isGraphicCardEnabled;
    private final boolean isBluetoothEnabled;

    private Computador(ComputadorBuilder builder) {
        this.HDD = builder.HDD;
        this.RAM = builder.RAM;
        this.isGraphicCardEnabled = builder.isGraphicCardEnabled;
        this.isBluetoothEnabled = builder.isBluetoothEnabled;
    }

    public String getHDD() {
        return HDD;
    }

    public String getRAM() {
        return RAM;
    }

    public boolean isGraphicCardEnabled() {
        return isGraphicCardEnabled;
    }

    public boolean isBluetoothEnabled() {
        return isBluetoothEnabled;
    }

    /**
     * Builder interno.
     * <p>
     * Perceba que a classe Computador tem apenas os métodos Getters e não possui um construtor publico,
     * então a única forma de obter um objeto Computador é através da classe ComputadorBuilder.
     */
    public static class ComputadorBuilder {
        // Atributos obrigatórios
        private final String HDD;
        private final String RAM;

        // Atributos opcionais
        private boolean isGraphicCardEnabled;
        private boolean isBluetoothEnabled;

        public ComputadorBuilder(String HDD, String RAM) {
            this.HDD = HDD;
            this.RAM = RAM;
        }

        public ComputadorBuilder setGraphicCardEnabled(boolean isGraphicCardEnabled) {
            this.isGraphicCardEnabled = isGraphicCardEnabled;
            return this;
        }

        public ComputadorBuilder setBluetoothEnabled(boolean isBluetoothEnabled) {
            this.isBluetoothEnabled = isBluetoothEnabled;
            return this;
        }

        public Computador build() {
            return new Computador(this);
        }
    }
}
