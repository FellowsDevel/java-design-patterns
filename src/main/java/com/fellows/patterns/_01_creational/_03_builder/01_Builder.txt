O padrão de design Builder é do tipo de Padrão de Criação (Creational Design Pattern).
Assim como os padrões Factory e Abstract Factory.
O padrão Builder foi intriduzido para resolver alguns dos problemas com os padrões Factory e Abstract Factory tipo
quando o Objeto possui diversos atributos.

Existem três grandes problemas ao utilizar os padrões Factory e Abstract Factory quando o Objeto contém muitos atributos:
1 - Muitos argumentos para passar do programa cliente à classe Factory que pode levar ao erro por causa que na
    maioria do tempo, o tipo dos argumentos são os mesmos e do lado do cliente, é difícil de manter a ordem dos argumentos.
2 - Alguns dos parâmetros podem ser opcionais mas no padrão Factory, somos forçados a enviar todos os parametros e
    parametros opcionais necessários sendo enviados como NULL.
3 - Se o Objeto é pesado e sua criação é complexa, então toda a complexidade também fará parte da Factory, o que
    torna bastante confuso.

Podemos resolver esses problemas com um grande número de parâmetros opcionais e estados inconsistentes ao provermos
uma forma de construir o objeto passo a passo e prover um método que retornará o objeto final.

