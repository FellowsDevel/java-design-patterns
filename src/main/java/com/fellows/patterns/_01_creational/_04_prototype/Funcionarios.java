package com.fellows.patterns._01_creational._04_prototype;

import java.util.ArrayList;
import java.util.List;

public class Funcionarios implements Cloneable {

    private final List<String> lista;

    public Funcionarios() {
        this.lista = new ArrayList<>();
    }

    public Funcionarios(List<String> lista) {
        this.lista = lista;
    }


    /**
     * Vamos causar um atraso na chamada para simular um acesso demorado ao banco de dados
     */
    public void loadData() {
        try {
            Thread.sleep(1000);
            this.lista.add("André");
            this.lista.add("João");
            this.lista.add("Pedro");
            this.lista.add("Luiz");
            this.lista.add("Daniel");
            this.lista.add("Rodrigo");

        } catch (InterruptedException ignore) {
        }

    }

    public List<String> getLista() {
        return lista;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        List<String> novo = new ArrayList<>();
        for (String item : this.getLista()) {
            novo.add(item);
        }
        return new Funcionarios(novo);
    }

}
