package com.fellows.patterns._01_creational._01_singleton;

import org.junit.Before;
import org.junit.Test;

import java.io.*;
import java.lang.reflect.Constructor;
import java.nio.file.Files;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@SuppressWarnings({ "rawtypes", "unchecked" })
public class LazyInitializationSingletonTest {

    private static void print(String name, Object object) {
        System.out.println(String.format("Objeto: %s, Hashcode: %d", name, object.hashCode()));
    }

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void getInstance() {
        LazyInitializationSingleton singleton1 = LazyInitializationSingleton.getInstance();
        LazyInitializationSingleton singleton2 = LazyInitializationSingleton.getInstance();

        assert singleton1.hashCode() == singleton2.hashCode();
    }

    /**
     * Utilizando reflexão podemos burlar o padrão Lazy Initialized Singleton
     */
    @Test
    public void singletonReflectionFailTest() throws Exception {

        LazyInitializationSingleton singleton1 = LazyInitializationSingleton.getInstance();
        LazyInitializationSingleton singleton2 = LazyInitializationSingleton.getInstance();

        print("Singleton 1", singleton1);
        print("Singleton 2", singleton2);

        assert singleton1.hashCode() == singleton2.hashCode();

        //
        // Fail case
        //
        Class clazz = Class.forName("com.fellows.patterns._01_creational._01_singleton.LazyInitializationSingleton");

        Constructor<LazyInitializationSingleton> constructor = clazz.getDeclaredConstructor();
        constructor.setAccessible(true);

        LazyInitializationSingleton singleton3 = constructor.newInstance();

        print("Singleton 3", singleton3);

        assert singleton1.hashCode() == singleton2.hashCode() &&
                singleton1.hashCode() != singleton3.hashCode() &&
                singleton2.hashCode() != singleton3.hashCode();

    }

    /**
     * Através de serialização/deserialização também conseguimos burlar o padrão Lazy Initialized Singleton
     */
    @Test
    public void singletonSerializationDeserializationFailTest() throws Exception {

        LazyInitializationSingleton singleton1 = LazyInitializationSingleton.getInstance();
        LazyInitializationSingleton singleton2 = LazyInitializationSingleton.getInstance();

        print("Singleton 1", singleton1);
        print("Singleton 2", singleton2);

        assert singleton1.hashCode() == singleton2.hashCode();

        //
        // Fail case
        //
        File tempFile = Files.createTempFile("singleton1", ".sng").toFile();
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(tempFile));
        objectOutputStream.writeObject(singleton1);

        ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(tempFile));
        LazyInitializationSingleton singleton3 = (LazyInitializationSingleton) objectInputStream.readObject();

        print("Singleton 3", singleton3);
        objectInputStream.close();
        objectOutputStream.close();

        assert singleton1.hashCode() == singleton2.hashCode() &&
                singleton1.hashCode() != singleton3.hashCode() &&
                singleton2.hashCode() != singleton3.hashCode();
    }

    /**
     * Podemos também burlar o padrão utilizando a funcionalidade de Clone
     */
    @Test
    public void singletonCloneFailTest() throws Exception {

        LazyInitializationSingleton singleton1 = LazyInitializationSingleton.getInstance();
        LazyInitializationSingleton singleton2 = LazyInitializationSingleton.getInstance();

        print("Singleton 1", singleton1);
        print("Singleton 2", singleton2);

        assert singleton1.hashCode() == singleton2.hashCode();

        //
        // Fail case
        //
        LazyInitializationSingleton singleton3 = (LazyInitializationSingleton) singleton1.clone();

        print("Singleton 3", singleton3);

        assert singleton1.hashCode() == singleton2.hashCode() &&
                singleton1.hashCode() != singleton3.hashCode() &&
                singleton2.hashCode() != singleton3.hashCode();
    }


    /**
     * Existe problemas também com multi thread
     * <p>
     * No OpenJDK 64-Bit Server VM (build 10.0.2+13-Ubuntu-1ubuntu0.18.04.3, mixed mode)
     * <p>
     * não consegui demonstrar o problema de multi thread com o Lazy Initialized Singleton
     */
    @Test
    public void singletonMultiThreadFailTest() throws Exception {
        LazyTestClass.init();

    }
}

class LazyTestClass {
    private static void print(String name, LazyInitializationSingleton object) {
        System.out.println(String.format("Objeto: %s, Hashcode: %d", name, object.hashCode()));
    }

    private static void useSingleton() {
        LazyInitializationSingleton singleton = LazyInitializationSingleton.getInstance();
        print("Singleton", singleton);
    }

    public static void init() throws Exception {
        ExecutorService executorService = Executors.newFixedThreadPool(6);

        executorService.submit(LazyTestClass::useSingleton);
        executorService.submit(LazyTestClass::useSingleton);
        executorService.submit(LazyTestClass::useSingleton);
        executorService.submit(LazyTestClass::useSingleton);
        executorService.submit(LazyTestClass::useSingleton);
        executorService.submit(LazyTestClass::useSingleton);

        executorService.shutdown();

    }

}
