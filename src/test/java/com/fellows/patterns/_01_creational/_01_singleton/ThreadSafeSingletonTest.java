package com.fellows.patterns._01_creational._01_singleton;

import org.junit.Before;
import org.junit.Test;

public class ThreadSafeSingletonTest {

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void getInstance() {
        ThreadSafeSingleton singleton1 = ThreadSafeSingleton.getInstance();
        ThreadSafeSingleton singleton2 = ThreadSafeSingleton.getInstance();

        assert singleton1.hashCode() == singleton2.hashCode();
    }

    @Test
    public void getInstanceDoubleLocking() {
        ThreadSafeSingleton singleton1 = ThreadSafeSingleton.getInstanceDoubleLocking();
        ThreadSafeSingleton singleton2 = ThreadSafeSingleton.getInstanceDoubleLocking();

        assert singleton1.hashCode() == singleton2.hashCode();
    }
}