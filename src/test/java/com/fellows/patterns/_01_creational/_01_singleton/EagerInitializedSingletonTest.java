package com.fellows.patterns._01_creational._01_singleton;

import org.junit.Before;
import org.junit.Test;

import java.io.*;
import java.lang.reflect.Constructor;
import java.nio.file.Files;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@SuppressWarnings({ "rawtypes", "unchecked" })
public class EagerInitializedSingletonTest {

    private static void print(String name, EagerInitializedSingleton object) {
        System.out.println(String.format("Objeto: %s, Hashcode: %d", name, object.hashCode()));
    }

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void getInstance() {
        EagerInitializedSingleton singleton1 = EagerInitializedSingleton.getInstance();
        EagerInitializedSingleton singleton2 = EagerInitializedSingleton.getInstance();

        assert singleton1.hashCode() == singleton2.hashCode();

    }

    /**
     * Utilizando reflexão podemos burlar o padrão Eager Initialized Singleton
     */
	@Test
    public void singletonReflectionFailTest() throws Exception {

        EagerInitializedSingleton singleton1 = EagerInitializedSingleton.getInstance();
        EagerInitializedSingleton singleton2 = EagerInitializedSingleton.getInstance();

        print("Singleton 1", singleton1);
        print("Singleton 2", singleton2);

        assert singleton1.hashCode() == singleton2.hashCode();

        //
        // Fail case
        //
        Class clazz = Class.forName("com.fellows.patterns._01_creational._01_singleton.EagerInitializedSingleton");

        Constructor<EagerInitializedSingleton> constructor = clazz.getDeclaredConstructor();
        constructor.setAccessible(true);

        EagerInitializedSingleton singleton3 = constructor.newInstance();

        print("Singleton 3", singleton3);

        assert singleton1.hashCode() == singleton2.hashCode() &&
                singleton1.hashCode() != singleton3.hashCode() &&
                singleton2.hashCode() != singleton3.hashCode();

    }

    /**
     * Através de serialização/deserialização também conseguimos burlar o padrão Eager Initialized Singleton
     */
    @Test
    public void singletonSerializationDeserializationFailTest() throws Exception {

        EagerInitializedSingleton singleton1 = EagerInitializedSingleton.getInstance();
        EagerInitializedSingleton singleton2 = EagerInitializedSingleton.getInstance();

        print("Singleton 1", singleton1);
        print("Singleton 2", singleton2);

        assert singleton1.hashCode() == singleton2.hashCode();

        //
        // Fail case
        //
        File tempFile = Files.createTempFile("singleton1", ".sng").toFile();
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(tempFile));
        objectOutputStream.writeObject(singleton1);

        ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(tempFile));
        EagerInitializedSingleton singleton3 = (EagerInitializedSingleton) objectInputStream.readObject();

        print("Singleton 3", singleton3);
        objectInputStream.close();
        objectOutputStream.close();

        assert singleton1.hashCode() == singleton2.hashCode() &&
                singleton1.hashCode() != singleton3.hashCode() &&
                singleton2.hashCode() != singleton3.hashCode();
    }

    /**
     * Podemos também burlar o padrão utilizando a funcionalidade de Clone
     */
    @Test
    public void singletonCloneFailTest() throws Exception {

        EagerInitializedSingleton singleton1 = EagerInitializedSingleton.getInstance();
        EagerInitializedSingleton singleton2 = EagerInitializedSingleton.getInstance();

        print("Singleton 1", singleton1);
        print("Singleton 2", singleton2);

        assert singleton1.hashCode() == singleton2.hashCode();

        //
        // Fail case
        //
        EagerInitializedSingleton singleton3 = (EagerInitializedSingleton) singleton1.clone();

        print("Singleton 3", singleton3);

        assert singleton1.hashCode() == singleton2.hashCode() &&
                singleton1.hashCode() != singleton3.hashCode() &&
                singleton2.hashCode() != singleton3.hashCode();
    }


    /**
     * Existe problemas também com multi thread
     * <p>
     * No OpenJDK 64-Bit Server VM (build 10.0.2+13-Ubuntu-1ubuntu0.18.04.3, mixed mode)
     * não consegui demonstrar o problema de multi thread com o Eager Initialized Singleton
     */
    @Test
    public void singletonMultiThreadFailTest() throws Exception {
        new TestClass().init();

    }
}

class TestClass {
    private static void print(String name, EagerInitializedSingleton object) {
        System.out.println(String.format("Objeto: %s, Hashcode: %d", name, object.hashCode()));
    }

    private static void useSingleton() {
        EagerInitializedSingleton singleton = EagerInitializedSingleton.getInstance();
        print("Singleton", singleton);
    }

    void init() throws Exception {
        ExecutorService executorService = Executors.newFixedThreadPool(6);

        executorService.submit(TestClass::useSingleton);
        executorService.submit(TestClass::useSingleton);
        executorService.submit(TestClass::useSingleton);
        executorService.submit(TestClass::useSingleton);
        executorService.submit(TestClass::useSingleton);
        executorService.submit(TestClass::useSingleton);

        executorService.shutdown();

    }

}
