package com.fellows.patterns._01_creational._03_builder;

import org.junit.Before;
import org.junit.Test;

public class ComputadorTest {

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void testComputadorBuilder() {

        Computador computador = new Computador.ComputadorBuilder("1 TB", "4 GB")
                .setBluetoothEnabled(true)
                .setGraphicCardEnabled(true)
                .build();

        assert computador.isBluetoothEnabled();
        assert computador.isGraphicCardEnabled();
        assert computador.getHDD().equalsIgnoreCase("1 TB");
        assert computador.getRAM().equalsIgnoreCase("4 GB");
    }

}