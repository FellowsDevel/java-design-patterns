package com.fellows.patterns._01_creational._02_factory.factories;

import com.fellows.patterns._01_creational._02_factory.enums.TipoComputador;
import com.fellows.patterns._01_creational._02_factory.factories.normalfactory.ComputadorFactory;
import com.fellows.patterns._01_creational._02_factory.model.Computador;
import com.fellows.patterns._01_creational._02_factory.model.Pc;
import com.fellows.patterns._01_creational._02_factory.model.Servidor;
import org.junit.Before;
import org.junit.Test;

public class ComputadorFactoryTest {

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void getComputadorPc() {
        Computador pc = ComputadorFactory.getComputador(TipoComputador.PC, "4 GB", "2 TB", "2.5 GHz");

        assert pc instanceof Pc;
    }

    @Test
    public void getComputadorServidor() {
        Computador servidor = ComputadorFactory.getComputador(TipoComputador.SERVIDOR, "32 GB", "128 TB", "5 GHz");

        assert servidor instanceof Servidor;
    }

}