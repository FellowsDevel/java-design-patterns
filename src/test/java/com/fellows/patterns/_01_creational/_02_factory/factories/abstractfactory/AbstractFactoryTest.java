package com.fellows.patterns._01_creational._02_factory.factories.abstractfactory;

import com.fellows.patterns._01_creational._02_factory.model.Computador;
import com.fellows.patterns._01_creational._02_factory.model.Pc;
import com.fellows.patterns._01_creational._02_factory.model.Servidor;
import org.junit.Before;
import org.junit.Test;

public class AbstractFactoryTest {

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void getComputadorPc() {
        Computador pc = AbstractFactory.getComputador(new PcFactory("4 GB", "1 TB", "2.5 GHz"));

        assert pc instanceof Pc;
    }

    @Test
    public void getComputadorServidor() {
        Computador servidor = AbstractFactory.getComputador(new ServidorFactory("32 GB", "128 TB", "5 GHz"));

        assert servidor instanceof Servidor;
    }
}