package com.fellows.patterns._01_creational._04_prototype;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class FuncionariosTest {

    @Before
    public void setUp() throws Exception {

    }

    @Test
    public void loadData() throws CloneNotSupportedException {

        Funcionarios lista = new Funcionarios();

        System.out.println("Carregando lista...");
        lista.loadData();
        System.out.println("Lista carregada!");

        // Agora podemos manipular os dados
        System.out.println("Criando lista f1 para manipulação");
        Funcionarios f1 = (Funcionarios) lista.clone();
        System.out.println("Lista f1 criada\n");

        System.out.println("Criando lista f1 para manipulação");
        Funcionarios f2 = (Funcionarios) lista.clone();
        System.out.println("Lista f2 criada\n");

        List<String> l1 = f1.getLista();
        l1.add("Zeus");

        List<String> l2 = f2.getLista();
        l2.add("Maomé");
        l2.remove("Pedro");

        System.out.println("Lista original: " + lista.getLista());
        System.out.println("Lista 1       : " + l1);
        System.out.println("Lista 2       : " + l2);

        assert lista.getLista().size() == 6;
        assert l1.size() == 7;
        assert l2.contains("Maomé");
    }

}