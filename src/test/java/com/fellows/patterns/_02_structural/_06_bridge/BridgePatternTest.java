package com.fellows.patterns._02_structural._06_bridge;

import org.junit.Test;

public class BridgePatternTest {

    @Test
    public void patternTest() {
        Shape tri = new Triangle(new RedColor());
        tri.applyColor();

        Shape pen = new Pentagon(new GreenColor());
        pen.applyColor();
    }
}