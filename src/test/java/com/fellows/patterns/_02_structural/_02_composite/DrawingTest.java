package com.fellows.patterns._02_structural._02_composite;

import org.junit.Test;

public class DrawingTest {

    @Test
    public void testeComposite() {
        Shape triangulo = new Triangle();
        Shape triangulo2 = new Triangle();
        Shape circulo = new Circle();
        Shape quadrado = new Square();

        Drawing drawing = new Drawing();

        drawing.add(triangulo);
        drawing.add(triangulo2);
        drawing.add(circulo);
        drawing.draw("Vermelho");

        drawing.clear();

        drawing.add(circulo);
        drawing.add(quadrado);
        drawing.draw("Azul");
    }
}