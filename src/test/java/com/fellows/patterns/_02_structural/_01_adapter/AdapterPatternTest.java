package com.fellows.patterns._02_structural._01_adapter;

import org.junit.Test;

public class AdapterPatternTest {

    private static Volt getVolt(SocketAdapter socketAdapter, int i) {
        switch (i) {
            case 3:
                return socketAdapter.get3Volt();
            case 12:
                return socketAdapter.get12Volt();
            default:
                return socketAdapter.get127Volt();
        }

    }

    @Test
    public void testObjectAdapter() {
        SocketAdapter socketAdapter = new SocketObjectAdapterImpl();
        Volt v3 = getVolt(socketAdapter, 3);
        Volt v12 = getVolt(socketAdapter, 12);
        Volt v127 = getVolt(socketAdapter, 127);

        System.out.println("3V utilizando Adaptador Objeto: " + v3.getVolts());
        System.out.println("12V utilizando Adaptador Objeto: " + v12.getVolts());
        System.out.println("127V utilizando Adaptador Objeto: " + v127.getVolts());

        assert v3.getVolts() == 3;
        assert v12.getVolts() == 12;
        assert v127.getVolts() == 127;
    }

    @Test
    public void testClassAdapter() {
        SocketAdapter socketAdapter = new SocketClassAdapterImpl();
        Volt v3 = getVolt(socketAdapter, 3);
        Volt v12 = getVolt(socketAdapter, 12);
        Volt v127 = getVolt(socketAdapter, 127);

        System.out.println("3V utilizando Adaptador Classe: " + v3.getVolts());
        System.out.println("12V utilizando Adaptador Classe: " + v12.getVolts());
        System.out.println("127V utilizando Adaptador Classe: " + v127.getVolts());

        assert v3.getVolts() == 3;
        assert v12.getVolts() == 12;
        assert v127.getVolts() == 127;
    }
}