package com.fellows.patterns._02_structural._03_proxy;

import org.junit.Test;

public class ProxyPatternTest {

    @Test
    public void patternTest() {
        CommandExecutor executor = new CommandExecutorProxy("Usuario","senha");
        try {
            executor.runCommand("ls -ltr");
            executor.runCommand(" rm -rf abc.pdf");
        } catch (Exception e) {
            System.out.println("Exception Message::"+e.getMessage());
            assert true;
        }
    }
}