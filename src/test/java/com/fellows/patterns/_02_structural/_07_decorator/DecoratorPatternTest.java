package com.fellows.patterns._02_structural._07_decorator;

import org.junit.Test;

public class DecoratorPatternTest {

    @Test
    public void patternTest() {
        Car sport = new SportsCar(new BasicCar());
        sport.assemble();
        System.out.println("\n***************");

        Car sportLuxuryCar = new SportsCar(new LuxuryCar(new BasicCar()));
        sportLuxuryCar.assemble();

    }
}