package com.fellows.patterns._02_structural._05_facade;

import org.junit.Test;

import java.sql.Connection;

public class FacadePatternTest {

    String tableName = "Funcionarios";

    @Test
    public void noFacadeTest() {

        // Gerando relatório MySql em HTML e Oracle PDF sem usar a Facade:
        Connection connMySql = MySqlHelper.getMySqlDBConnection();
        MySqlHelper mySqlHelper = new MySqlHelper();
        mySqlHelper.generateMySqlHTMLReport(tableName, connMySql);

        Connection connOracle = OracleHelper.getOracleDBConnection();
        OracleHelper oracleHelper = new OracleHelper();
        oracleHelper.generateOraclePDFReport(tableName, connOracle);
    }

    /**
     * Forma bem mais limpa de utilização
     */
    @Test
    public void facadeTest() {

        // Gerando relatório MySql em HTML e Oracle PDF usando a Facade:
        HelperFacade.generateReport(HelperFacade.DBType.MYSQL, HelperFacade.ReportType.HTML, tableName);
        HelperFacade.generateReport(HelperFacade.DBType.ORACLE, HelperFacade.ReportType.PDF, tableName);
    }
}